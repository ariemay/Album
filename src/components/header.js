//Import libraries
import React from 'react';
import { Text, View } from 'react-native';


//Make component
const Header = (props) => {
    const { textStyle, viewStyle } = styles;
    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.judul}</Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#F8F8F8',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        elevation: 5,
    },
    textStyle: {
        fontSize: 30,
        fontFamily: 'Pacifico',
        color: 'black',
        marginTop: 5,
        marginBottom: 5,
    }
};

//Make the component aailable to other parts of the app
export default Header;
